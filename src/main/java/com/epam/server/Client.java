package com.epam.server;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    public static void main(String[] args) {
        int severPort = 8090;
        String address = "127.0.0.2";
        try {
            InetAddress ipAddress = InetAddress.getByName(address);
            System.out.println("IpAddress: " + address + " and port: " + severPort);

            Socket socket = new Socket(ipAddress, severPort);
            System.out.println(socket);
            System.out.println(socket.isConnected());

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            String line = null;

            line = keyboard.readLine();

            byte[] message = (line).getBytes();
            in.read(message);

            byte[] buf = new byte[100];
            out.write(buf);

            System.out.println(buf);

            Thread.sleep(2000);
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
