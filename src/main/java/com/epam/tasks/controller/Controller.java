package com.epam.tasks.controller;

public interface Controller {

    void createDroidShip();

    void readByUsualAndBufferedReader();

    void pushDataToStream();

    void readJavaFile();

    void displayContentOfDirectory();

    void readAndWriteByNIO();

}


