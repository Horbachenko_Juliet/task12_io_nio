package com.epam.tasks.controller;


import com.epam.tasks.model.Model;
import com.epam.tasks.model.ModelImpl;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }

    @Override
    public void createDroidShip() {
        model.createDroidShip();
    }

    @Override
    public void readByUsualAndBufferedReader() {
        model.readByUsualAndBufferedReader();
    }

    @Override
    public void pushDataToStream() {
        model.pushDataToStream();
    }

    @Override
    public void readJavaFile() {
        model.readJavaFile();
    }

    @Override
    public void displayContentOfDirectory() {
        model.displayContentOfDirectory();
    }

    @Override
    public void readAndWriteByNIO() {
        model.readAndWriteByNIO();
    }
}