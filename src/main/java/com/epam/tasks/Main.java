package com.epam.tasks;

import com.epam.tasks.view.MyView;

public class Main {
    public static void main(String[] args) {
        new MyView().show();
    }
}
