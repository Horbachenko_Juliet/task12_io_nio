package com.epam.tasks.model.droid;

import com.epam.tasks.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {

    private String name;
    private transient String type;
    private List<Droid> droids;
    public static Logger logger = LogManager.getLogger(MyView.class);

    public Ship(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public List<Droid> fillShip(){
        droids = new ArrayList<>();
        droids.add(new Droid("Defence droid",10,150));
        droids.add(new Droid("Protect droid",11,250));
        for (int i = 0; i < 5; i++) {
            droids.add(new Droid("Combat Droid", 100 + i, 300));
        }
       return droids;
    }

    public void serializeShip(){
        try(ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream("droids.ser"))) {
            for (Droid droid : droids) {
                ous.writeObject(droid);
            }
            System.out.println("Serialized data is saved in droids.ser");
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Error in serialize method");
        }
    }

    public void deserializeShip(){
        Droid d;
        List<Droid> deserializeList = new ArrayList<>();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("droids.ser"))) {
            deserializeList.add(d = (Droid) ois.readObject());
            System.out.print("Name of first droid in ship is : ");
            System.out.println(d.getName());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("Error in deserialize method");
        }
    }
}
