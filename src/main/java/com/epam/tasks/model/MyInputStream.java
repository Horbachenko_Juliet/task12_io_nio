package com.epam.tasks.model;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class MyInputStream {

    final PipedInputStream pipedInputStream = new PipedInputStream();
    final PipedOutputStream pipedOutputStream = new PipedOutputStream();

    public void createMyInputStream() {
        try {
            pipedInputStream.connect(pipedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread pipeWriter = new Thread(() -> {
            for (int i = 65; i < 91; i++) {
                try {
                    pipedOutputStream.write(i);
                    Thread.sleep(250);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread pipeReader = new Thread(() -> {
            for (int i = 65; i < 91; i++) {
                try {
                    pipedInputStream.read();
                    Thread.sleep(500);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        pipeWriter.start();
        pipeReader.start();
        try {
            pipeWriter.join();
        pipeReader.join();
        pipedOutputStream.close();
        pipedInputStream.close();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }
}
