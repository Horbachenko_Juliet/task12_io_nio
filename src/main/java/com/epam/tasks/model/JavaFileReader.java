package com.epam.tasks.model;

import java.io.*;

public class JavaFileReader {
    public String findComment(String filename) {
        File file = new File(filename);
        String comments = " ";
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file))) {
            int data = 1;
            while (data != -1) {
                data = bufferedInputStream.read();
                if (data == '"') {
                    data = bufferedInputStream.read();
                    while (data != '"') {
                        data = bufferedInputStream.read();
                    }
                }
                if (data == '/') {
                    data = bufferedInputStream.read();
                    if (data == '/') {
                        comments += commentProcessing(bufferedInputStream,'\n');
                    } else if (data == '*') {
                        comments += commentProcessing(bufferedInputStream,'*','/');
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return comments;
    }


    private boolean checkComparing( BufferedInputStream bufferedInputStream, int data, char ... args) {
        for (char ch : args) {
            if (data != ch) return false;
            try {
                data = bufferedInputStream.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private String commentProcessing(BufferedInputStream bufferedInputStream, char... chars) {
        String comment = "";
        int data = 1;
        try {
            do {
                comment += (char) data;
                data = bufferedInputStream.read();
                if (data == chars[0]) {
                    if (checkComparing(bufferedInputStream, data, chars)) {
                        break;
                    }
                }
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return comment + "\n";
    }
}
