package com.epam.tasks.model.reader;

import java.io.*;
import java.util.Date;

public class MyReader {

    long timeCount;

    public void compareReaderPerformance(String filePath) {
        int count = 0;
        try (InputStream inputStream = new FileInputStream(filePath)) {
            Date start = new Date();
            int data = inputStream.read();
            while (data != -1) {
                data = inputStream.read();
                count++;
            }
            Date end = new Date();
            timeCount = end.getTime() - start.getTime();
        } catch (IOException e) {
            e.printStackTrace();

        }
        System.out.println("count = " + count);
        System.out.println("End usual reader for: " + timeCount + "ms");

        count = 0;
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(filePath))) {
            Date start = new Date();
            int data = bufferedInputStream.read();
            while (data != -1) {
                data = bufferedInputStream.read();
                count++;
            }
            Date end = new Date();
            timeCount = end.getTime() - start.getTime();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("count = " + count);
        System.out.println("End buffered reader for: " + timeCount + "ms");
    }

    public void compareWriterPerformance(String filePath) {
        int count = 0;
        try (OutputStream outputStream = new FileOutputStream("fileResult.txt");
             BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(filePath))) {
            Date start = new Date();
            int data = bufferedInputStream.read();
            while (data != -1) {
                data = bufferedInputStream.read();
                outputStream.write(data);
                count++;
            }
            Date end = new Date();
            timeCount = end.getTime() - start.getTime();
        } catch (IOException e) {
            e.printStackTrace();

        }
        System.out.println("count = " + count);
        System.out.println("End usual writer for: " + timeCount + "ms");

        count = 0;
        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream("fileResult.txt"));
             BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(filePath))) {
            Date start = new Date();
            int data = bufferedInputStream.read();
            while (data != -1) {
                data = bufferedInputStream.read();
                bufferedOutputStream.write(data);
                count++;
            }
            Date end = new Date();
            timeCount = end.getTime() - start.getTime();
        } catch (IOException e) {
            e.printStackTrace();

        }
        System.out.println("count = " + count);
        System.out.println("End buffered writer for: " + timeCount + "ms");
    }
}


